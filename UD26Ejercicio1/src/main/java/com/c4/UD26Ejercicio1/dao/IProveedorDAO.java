package com.c4.UD26Ejercicio1.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD26Ejercicio1.dto.Proveedor;

public interface IProveedorDAO extends JpaRepository<Proveedor, String>{

}
