package com.c4.UD26Ejercicio4.service;

import java.util.List;

import com.c4.UD26Ejercicio4.dto.Investigadores;


public interface IInvestigadoresService {
	
	public List<Investigadores> listarInvestigadores();
	
	public Investigadores guardarInvestigadores(Investigadores investigadores);
	
	public Investigadores investigadoresXID(String id);
	
	public Investigadores actualizarInvestigadores(Investigadores investigadores);
	
	public void eliminarInvestigadores(String id);

}
