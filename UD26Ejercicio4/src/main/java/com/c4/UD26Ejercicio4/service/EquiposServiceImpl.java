package com.c4.UD26Ejercicio4.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD26Ejercicio4.dao.IEquiposDAO;
import com.c4.UD26Ejercicio4.dto.Equipos;

@Service
public class EquiposServiceImpl implements IEquiposService{
	
	@Autowired
	IEquiposDAO iEquiposDAO;

	@Override
	public List<Equipos> listarEquipos() {
		return iEquiposDAO.findAll();
	}

	@Override
	public Equipos guardarEquipos(Equipos equipos) {
		return iEquiposDAO.save(equipos);
	}

	@Override
	public Equipos equiposXID(String id) {
		return iEquiposDAO.findById(id).get();
	}

	@Override
	public Equipos actualizarEquipos(Equipos equipos) {
		return iEquiposDAO.save(equipos);
	}

	@Override
	public void eliminarEquipos(String id) {
		iEquiposDAO.deleteById(id);
		
	}




}
