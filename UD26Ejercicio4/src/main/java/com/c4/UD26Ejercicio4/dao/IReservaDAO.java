package com.c4.UD26Ejercicio4.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD26Ejercicio4.dto.Reserva;

public interface IReservaDAO extends JpaRepository<Reserva, Long> {

}
