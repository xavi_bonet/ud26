package com.c4.UD26Ejercicio4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD26Ejercicio4.dto.Investigadores;
import com.c4.UD26Ejercicio4.service.InvestigadoresServiceImpl;

@RestController
@RequestMapping("/api")
public class InvestigadoresController {
	
	@Autowired
	InvestigadoresServiceImpl investigadoresServiceImpl;
	
	@GetMapping("/investigadores")
	public List<Investigadores> listarInvestigadores(){
		return investigadoresServiceImpl.listarInvestigadores();
	}
	
	@PostMapping("/investigadores")
	public Investigadores salvarInvestigadores(@RequestBody Investigadores investigadores) {
		return investigadoresServiceImpl.guardarInvestigadores(investigadores);
	}
		
	@GetMapping("/investigadores/{id}")
	public Investigadores investigadoresXID(@PathVariable(name="id") String id) {
		
		Investigadores investigadores_xid= new Investigadores();
		
		investigadores_xid = investigadoresServiceImpl.investigadoresXID(id);
		
		System.out.println("Investigadores XID: "+investigadores_xid);
		
		return investigadores_xid;
	}
	
	@PutMapping("/investigadores/{id}")
	public Investigadores actualizarInvestigadores(@PathVariable(name="id")String id,@RequestBody Investigadores investigadores) {
		
		Investigadores investigadores_seleccionado= new Investigadores();
		Investigadores investigadores_actualizado= new Investigadores();
		
		investigadores_seleccionado= investigadoresServiceImpl.investigadoresXID(id);
		
		investigadores_seleccionado.setNombre(investigadores.getNombre());
		investigadores_seleccionado.setDni(investigadores.getDni());
		investigadores_seleccionado.setFacultad(investigadores.getFacultad());
		
		investigadores_actualizado = investigadoresServiceImpl.actualizarInvestigadores(investigadores_seleccionado);
		
		System.out.println("El Investigador actualizado es: "+ investigadores_actualizado);
		
		return investigadores_actualizado;
	}
	
	@DeleteMapping("/investigadores/{id}")
	public void eleiminarInvestigadores(@PathVariable(name="id")String id) {
		investigadoresServiceImpl.eliminarInvestigadores(id);
	}
	
}
