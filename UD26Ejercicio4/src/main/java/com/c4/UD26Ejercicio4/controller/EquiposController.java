package com.c4.UD26Ejercicio4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD26Ejercicio4.dto.Equipos;
import com.c4.UD26Ejercicio4.service.EquiposServiceImpl;

@RestController
@RequestMapping("/api")
public class EquiposController {
	
	@Autowired
	EquiposServiceImpl equiposServiceImpl;
	
	@GetMapping("/equipos")
	public List<Equipos> listarEquipos(){
		return equiposServiceImpl.listarEquipos();
	}
	
	@PostMapping("/equipos")
	public Equipos salvarEquipos(@RequestBody Equipos equipos) {
		return equiposServiceImpl.guardarEquipos(equipos);
	}
		
	@GetMapping("/equipos/{id}")
	public Equipos equiposXID(@PathVariable(name="id") String id) {
		
		Equipos equipos_xid= new Equipos();
		
		equipos_xid = equiposServiceImpl.equiposXID(id);
		
		System.out.println("Equipos XID: "+equipos_xid);
		
		return equipos_xid;
	}
	
	@PutMapping("/equipos/{id}")
	public Equipos actualizarEquipos(@PathVariable(name="id")String id,@RequestBody Equipos equipos) {
		
		Equipos equipos_seleccionado= new Equipos();
		Equipos equipos_actualizado= new Equipos();
		
		equipos_seleccionado= equiposServiceImpl.equiposXID(id);
		
		equipos_seleccionado.setId(equipos.getId());
		equipos_seleccionado.setNombre(equipos.getNombre());
		equipos_seleccionado.setFacultad(equipos.getFacultad());
		
		equipos_actualizado = equiposServiceImpl.actualizarEquipos(equipos_seleccionado);
		
		System.out.println("El Equipo actualizado es: "+ equipos_actualizado);
		
		return equipos_actualizado;
	}
	
	@DeleteMapping("/equipos/{id}")
	public void eleiminarEquipos(@PathVariable(name="id")String id) {
		equiposServiceImpl.eliminarEquipos(id);
	}
	
}
