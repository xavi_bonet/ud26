package com.c4.UD26Ejercicio4.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="reserva")
public class Reserva {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private Long id;
	
	@Column(name = "comienzo")
	private Date comienzo;
	
	@Column(name = "fin")
	private Date fin;

	@ManyToOne
	@JoinColumn(name = "dni")
	Investigadores investigadores; 
	
	@ManyToOne
	@JoinColumn(name = "num_equipo")
	Equipos equipos;
	
	
    //Constructores
	public Reserva() {
		
	}

	public Reserva(Long id, Investigadores cientifico, Equipos equipos, Date comienzo, Date fin) {
		this.id = id;
		this.investigadores = cientifico;
		this.equipos = equipos;
		this.comienzo = comienzo;
		this.fin = fin;
	}
	
	//Metodos SET y GET

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the investigadores
	 */
	public Investigadores getInvestigadores() {
		return investigadores;
	}

	/**
	 * @param investigadores the investigadores to set
	 */
	public void setInvestigadores(Investigadores cientifico) {
		this.investigadores = cientifico;
	}

	/**
	 * @return the equipos
	 */
	public Equipos getEquipos() {
		return equipos;
	}

	/**
	 * @param equipos the equipos to set
	 */
	public void setEquipos(Equipos equipos) {
		this.equipos = equipos;
	}

	/**
	 * @return the comienzo
	 */
	public Date getComienzo() {
		return comienzo;
	}

	/**
	 * @param comienzo the comienzo to set
	 */
	public void setComienzo(Date comienzo) {
		this.comienzo = comienzo;
	}

	/**
	 * @return the fin
	 */
	public Date getFin() {
		return fin;
	}

	/**
	 * @param fin the fin to set
	 */
	public void setFin(Date fin) {
		this.fin = fin;
	}
	
	


	
}
