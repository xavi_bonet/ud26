package com.c4.UD26Ejercicio4.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="facultad")
public class Facultad {
	
	@Id
	@Column(name = "id_facultad")
	private Long id;
	@Column(name = "nombre")
	private String nombre;
	
    @OneToMany
    @JoinColumn(name="id_facultad")
    private List<Equipos> Equipos;
    
    @OneToMany
    @JoinColumn(name="id_facultad")
    private List<Investigadores> Investigadores;
	
    //Constructores
	public Facultad() {
		
	}

	public Facultad(Long id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	
	//Metodos SET y GET

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	/**
	 * @return the equipos
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "equipos")
	public List<Equipos> getEquipos() {
		return Equipos;
	}

	/**
	 * @param equipos the equipos to set
	 */
	public void setEquipos(List<Equipos> equipos) {
		Equipos = equipos;
	}

	/**
	 * @return the investigadores
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "investigadores")
	public List<Investigadores> getInvestigadores() {
		return Investigadores;
	}

	/**
	 * @param investigadores the investigadores to set
	 */
	public void setInvestigadores(List<Investigadores> investigadores) {
		Investigadores = investigadores;
	}
	
	
	
}
