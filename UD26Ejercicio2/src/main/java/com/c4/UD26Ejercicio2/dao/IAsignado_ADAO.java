package com.c4.UD26Ejercicio2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD26Ejercicio2.dto.Asignado_A;

public interface IAsignado_ADAO extends JpaRepository<Asignado_A, Long> {

}
