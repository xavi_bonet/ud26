package com.c4.UD26Ejercicio2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD26Ejercicio2.dto.Cientificos;

public interface ICientificosDAO extends JpaRepository<Cientificos, String> {

}
