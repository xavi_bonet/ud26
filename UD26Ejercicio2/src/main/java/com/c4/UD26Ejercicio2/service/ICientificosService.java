package com.c4.UD26Ejercicio2.service;

import java.util.List;

import com.c4.UD26Ejercicio2.dto.Cientificos;


public interface ICientificosService {
	
	public List<Cientificos> listarCientificos();
	
	public Cientificos guardarCientifico(Cientificos cientifico);
	
	public Cientificos cientificoXID(String id);
	
	public Cientificos actualizarCientifico(Cientificos cientifico);
	
	public void eliminarCientifico(String id);

}
