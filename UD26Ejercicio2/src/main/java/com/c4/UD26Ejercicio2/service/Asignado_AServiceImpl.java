package com.c4.UD26Ejercicio2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD26Ejercicio2.dao.IAsignado_ADAO;
import com.c4.UD26Ejercicio2.dto.Asignado_A;

@Service
public class Asignado_AServiceImpl implements IAsignado_AService{
	
	@Autowired
	IAsignado_ADAO iAsignado_ADAO;

	@Override
	public List<Asignado_A> listarAsignado_A() {
		return iAsignado_ADAO.findAll();
	}

	@Override
	public Asignado_A guardarAsignado_A(Asignado_A asignado_A) {
		return iAsignado_ADAO.save(asignado_A);
	}

	@Override
	public Asignado_A asignado_AXID(Long id) {
		return iAsignado_ADAO.findById(id).get();
	}

	@Override
	public Asignado_A actualizarAsignado_A(Asignado_A asignado_A) {
		return iAsignado_ADAO.save(asignado_A);
	}

	@Override
	public void eliminarAsignado_A(Long id) {
		iAsignado_ADAO.deleteById(id);
	}



}
