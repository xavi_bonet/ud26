package com.c4.UD26Ejercicio2.service;

import java.util.List;

import com.c4.UD26Ejercicio2.dto.Proyecto;


public interface IProyectoService {
	
	public List<Proyecto> listarProyectos();
	
	public Proyecto guardarProyecto(Proyecto proyecto);
	
	public Proyecto proyectoXID(String id);
	
	public Proyecto actualizarProyecto(Proyecto proyecto);
	
	public void eliminarProyecto(String id);

}
