package com.c4.UD26Ejercicio2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud26SpringMysqlEj2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud26SpringMysqlEj2Application.class, args);
	}

}
