package com.c4.UD26Ejercicio3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD26Ejercicio3.dto.Maquina;
import com.c4.UD26Ejercicio3.service.MaquinaServiceImpl;

@RestController
@RequestMapping("/api")
public class MaquinaController {

	@Autowired
	MaquinaServiceImpl maquinaServiceImpl;
	
	@GetMapping("/maquinas")
	public List<Maquina> listarMaquina(){
		return maquinaServiceImpl.listarMaquina();
	}
	
	@PostMapping("/maquina")
	public Maquina salvarMaquina(@RequestBody Maquina maquina) {
		return maquinaServiceImpl.guardarMaquina(maquina);
	}
	
	@GetMapping("/maquina/{id}")
	public Maquina maquinaXID(@PathVariable(name="id") int id) {
		
		Maquina maquina_xid= new Maquina();
		
		maquina_xid = maquinaServiceImpl.maquinaXID(id);
		
		System.out.println("Maquina XID: "+maquina_xid);
		
		return maquina_xid;
	}
	
	@PutMapping("/maquina/{id}")
	public Maquina actualizarMaquina(@PathVariable(name="id")int id,@RequestBody Maquina maquina) {
		
		Maquina maquina_seleccionado= new Maquina();
		Maquina maquina_actualizado= new Maquina();
		
		maquina_seleccionado= maquinaServiceImpl.maquinaXID(id);
		
		maquina_seleccionado.setPiso(maquina.getPiso());
		
		maquina_actualizado = maquinaServiceImpl.actualizarMaquina(maquina_seleccionado);
		
		System.out.println("La Maquina actualizada es: "+ maquina_actualizado);
		
		return maquina_actualizado;
	}
	
	@DeleteMapping("/maquina/{id}")
	public void eleiminarMaquina(@PathVariable(name="id")int id) {
		maquinaServiceImpl.eliminarMaquina(id);
	}
	
}
