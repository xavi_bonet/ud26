package com.c4.UD26Ejercicio3.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD26Ejercicio3.dto.Maquina;

public interface IMaquinaDAO extends JpaRepository<Maquina,Integer> {

}
